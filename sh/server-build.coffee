glob = require "glob"
html2jade = require 'html2jade'
fs = require 'fs'
path = require 'path'

serverDist = "dist-server"
baseUrl = "baseUrl"
baseUrl = "'http://static2.mgbeta.ru/nata/sheeps/'"

glob "dist/*.html", {mark: true}, (err, files) ->
  if err then throw err
  files.forEach (filename)->
    require('fs').readFile filename, (err, data) ->
      if err then throw err

      replaces = [
        [
          /<!--\s*block:([^>]*)\s*-->/g,
          "<block---$1>"
        ],
        [
          /<!--\s*endblock:([^>]*)\s*-->/g,
          "</block---$1>"
        ],
        [
          /block---/g,
          "block "
        ],
        [
          /(href=|src=)(\s*['"][^h/])/g,
#          /(href=|src=)/g,
          "$1" + baseUrl + "+$2"
        ]
      ]
      html = data.toString().replace(replaces[0][0], replaces[0][1]).replace(replaces[1][0], replaces[1][1])

      html2jade.convertHtml html, {}, (err, jade) ->
        final = jade.replace(replaces[2][0], replaces[2][1]).replace(replaces[3][0], replaces[3][1])

        basename = path.basename filename, ".html"
        filenameDist = serverDist + "/" + basename + ".jade"

        fs.writeFile filenameDist, final


#    require('path').basename('/foo/bar/baz/asdf/quux.html', '.html')
#  console.log files


