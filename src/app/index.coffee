angular.module "sheepFront", ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'ui.bootstrap']
.config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
  .state "home",
    url: "/",
    templateUrl: "app/main/main.html",
    controller: "MainCtrl"
  .state "sheep",
    url: "/sheep"
    templateUrl: "app/sheep/sheep.html"
    controller: "SheepCtrl"
  .state "user",
    url: "/user/:id"
    templateUrl: "app/user/user.html"
    controller: "UserCtrl"
  .state "users",
    url: "/users"
    templateUrl: "app/users/users.html"
    controller: "UsersCtrl"
  .state "profile",
    url: "/profile"
    templateUrl: "app/user/user.html"
    controller: "UserCtrl"

  $urlRouterProvider.otherwise '/'

.config (RestangularProvider, $httpProvider) ->
  $httpProvider.defaults.withCredentials = true
  RestangularProvider.setDefaultHeaders({
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  })
  serv_url = if window.location.hostname == 'localhost' then 'http://localhost:1337/' else 'http://sheep.mgbeta.ru/';

  RestangularProvider.setBaseUrl(serv_url);

.run ($rootScope) ->
  $rootScope.debug = false;
  $rootScope.isDebug = () -> $rootScope.debug
  $rootScope.toggleDebug = () ->
    if $rootScope.debug then $rootScope.debug = false else $rootScope.debug = true
