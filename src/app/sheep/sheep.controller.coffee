angular.module "sheepFront"
.filter "sheared", () ->
  (input) ->
    if input
      control = new Date().getTime() - 5 * 60 * 1000
      new Date(input).getTime() >= control
    else
      false
.directive 'thing', ->
  {
  restrict: 'E'
  template: '<div class="thing"><img class="sheep-100px" /><div><p class="text-center">{{sheep.name}}</p></div></div>'
  replace: true
  link: ($scope, element, attrs) ->
    transitionDurations = [
      'transitionDuration'
      'msTransitionDuration'
      'webkitTransitionDuration'
      'mozTransitionDuration'
      'oTransitionDuration'
    ]
    getSupportedPropertyName = (properties) ->
      i = 0
      while i < properties.length
        if typeof document.body.style[properties[i]] != 'undefined'
          return properties[i]
        i++
      null

    transforms = [
      'transform'
      'msTransform'
      'webkitTransform'
      'mozTransform'
      'oTransform'
    ]

    transitionDurationProperty = getSupportedPropertyName(transitionDurations)


    element.find('img').attr 'src', attrs.src

    setInitialPosition = ->
      updatePosition element
      setTimeout kickOffTransition, 100

    kickOffTransition = ->
      element.bind 'transitionend', ->
        updatePosition element
      element.bind 'webkitTransitionEnd', ->
        console.log('transitionend')
        updatePosition element
      element.bind 'mozTransitionEnd', ->
        updatePosition element
      element.bind 'msTransitionEnd', ->
        updatePosition element
      element.bind 'oTransitionEnd', ->
        updatePosition element

      setTranslate3DTransform element
      setTransitionDuration element

    updatePosition = (e) ->
      setTranslate3DTransform e
      setTransitionDuration e
      return

    getRandomXPosition = ->
      Math.round -50 + Math.random() * 900

    getRandomYPosition = ->
      Math.round -50 + Math.random() * 800

    getRandomDuration = ->
      3 + Math.random() * 3 + 's'

    setTranslate3DTransform = (element) ->
      element.css('transform', 'translate3d(' + getRandomXPosition() + 'px' + ', ' + getRandomYPosition() + 'px' + ', 0)')

    setTransitionDuration = (element) ->
      if transitionDurationProperty
        element.css('transitionDuration', getRandomDuration())

    setInitialPosition()

#    attrs.$observe 'caption', (value) ->
#      element.find('figcaption').text value
#    attrs.$observe 'photoSrc', (value) ->
#      element.find('img').attr 'src', value
  }

.controller "SheepCtrl", ($scope, Restangular, server) ->
  $scope.server = server;

#  Restangular.one('auth', 'external').one('{"username": "54d1ed7b660967c67bcf0309"}').get()
  #     Restangular.one('user', '54d1ed7b660967c67bcf0309').get().$object
  $scope.sheeps = Restangular.all('sheep').getList().$object

