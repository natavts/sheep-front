angular.module('sheepFront').service 'server', ($rootScope, Restangular) ->
  # AngularJS will instantiate a singleton by calling "new" on this function
  @sheep =
    allSheepObj: Restangular.all('sheep').getList().$object
    all: ->
      Restangular.all('sheep').getList()
      return
    one: (id) ->
      Restangular.one 'sheep', id
    cut: (id) ->
#      console.log this
      @one(id).one('cut').get().then (data) ->
        alert data.response
        #user.updateMyUserObj()
        return
      return
  @user =
    userPromise: ->
      Restangular.one('user', 'my').get()
    one: (id) ->
      Restangular.one('user', id).get()
    allUsers: ->
      Restangular.one('user').getList()
    myUserObj: Restangular.one(@myId).get().$object
    updateMyUserObj: ->
      self = this
#      console.log this
      @myUserObj = @one(self.myId).$object
      return
  @action = cut: (id) ->
    sheep.cut id
    return
  user = @user
  sheep = @sheep
  return
